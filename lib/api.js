const express = require("express");
const aws = require("aws-sdk");
const multer = require("multer");
const fs = require("fs");

const app = express();
const router = express.Router();

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "uploads/");
  },
  filename: function(req, file, cb) {
    cb(null, file.originalname);
  }
});

const upload = multer({ storage: storage });

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
  res.header("Access-Control-Max-Age", "3600");
  res.header("Access-Control-Allow-Headers", "Content-Type, Range, x-amz-acl");
  res.header(
    "Access-Control-Expose-Headers",
    "Accept-Ranges, Content-Encoding, Content-Length, Content-Range"
  );
  res.header("Access-Control-Allow-Credentials", "true");
  next();
});

app.options("*", (req, res) => {
  res.send(201);
});

const spacesEndpoint = new aws.Endpoint("sfo2.digitaloceanspaces.com");

const s3 = new aws.S3({
  endpoint: spacesEndpoint,
  accessKeyId: "GAXNKBIE36YDRGOISHOQ",
  secretAccessKey: "tTl84Y6VC7+juHgzGIpuqoIKVoKCzvP6TixHKQ5bYnw",
  region: "sfo2",
  signatureVersion: "v4"
});

router
  .route("/")
  .get((req, res) => {
    res.send("Upload endpoint");
  })

  .post(upload.single("images"), async (req, res) => {
    try {
      fs.readFile(req.file.path, function(err, data) {
        const s3Params = {
          Bucket: "2019phoenix",
          Key: `media/${req.file.originalname}`,
          ContentType: req.file.mimetype,
          Body: data,
          ACL: "public-read"
        };

        s3.upload(s3Params, function(err, data) {
          fs.unlink(req.file.path, function(err) {
            if (err) {
              throw new Error(err);
            }
          });

          if (err) {
            res.status(400).json({ status: false, message: err });
          } else {
            res.status(201).json({ status: true, data });
          }
        });
      });
    } catch (err) {
      res.status(400).json({ status: false, message: err });
    }
  });

app.use("/upload", router);

app.listen(3000);
